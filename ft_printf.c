/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: earruaba <earruaba@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/13 16:19:17 by earruaba          #+#    #+#             */
/*   Updated: 2021/10/15 16:04:20 by earruaba         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

int ft_printf(const char *str, ...);

int ft_strlen(char *s)
{
	int i = 0;
	if (!s)
		return (i);
	else
		while (s[i])
			i++;
	return (i);
}

int ft_putchar(char c)
{
	return (write(1, &c, 1));
}

int ft_putstr(char *s)
{
	if (!s)
		return (write(1,"(null)",6));
	return (write(1, s, ft_strlen(s)));
}

int ft_putnbr(int nb)
{
	unsigned int n;
	int tab[11];
	int len = 0;
	int i = 0;

	n = nb;
	if (nb < 0)
	{
		n = -nb;
		ft_putchar('-');
		len++;
	}
	while (n > 9)
	{
		tab[i] = n % 10 + '0';
		n /= 10;
		i++;
	}
	tab[i] = n + '0';
	while (i >= 0)
	{

		ft_putchar(tab[i--]);
		len++;
	}
	return (len);
}

int ft_puthex(unsigned int n)
{
	int len = 0;
	int i = 0;
	int tab[11];
	char *base = "0123456789abcdef";
	while (n > 9)
	{
		tab[i] = base[n % 16];
		n /= 16;
		i++;
	}
	if (base[n] == '0' && i != 0)
		i--;
	else
		tab[i] = base[n];
	while (i >= 0)
	{

		ft_putchar(tab[i--]);
		len++;
	}
	return (len);
}

int ft_parser(const char *str, va_list ap)
{
	if (*str == 's')
		return (ft_putstr(va_arg(ap, char *)));
	else if (*str == 'c')
		return (ft_putchar(va_arg(ap, int)));
	else if (*str == 'd')
		return (ft_putnbr(va_arg(ap, int)));
	else if (*str == 'x')
		return (ft_puthex(va_arg(ap, unsigned int)));
	else
		return (0);
}

int ft_printf(const char *str, ...)
{
	va_list ap;
	int len = 0;
	va_start(ap, str);
	while (*str)
	{
		if (*str == '%')
		{
			str++;
			len += ft_parser(str++, ap);
		}
		else
			len += write(1, str++, 1);
	}
	va_end(ap);
	return (len);
}
/*
int main()
{
	int ret = 0;

	printf("printf test c : %c\n", 'b');
	ft_printf("ft_ test c : %c\n", 'b');

	printf("printf stest d : %d\n", 42);
	ft_printf("ft_ test d : %d\n", 42);

	printf("printf test x : %x\n", 15);
	ft_printf("ft_ test x : %x\n", 15);

	printf("printf test s : %s\n", "Hello World");
	ft_printf("ft_ test s : %s\n", "Hello World");
	return (0);
}*/